package com.linpro.redpill.integration.protocol.sftp;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicBoolean;

import com.linpro.redpill.integration.engine.mule.connectors.sftp.rlsftp.config.RlsftpConnectorConfig;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mule.api.MuleRuntimeException;
import org.mule.transport.sftp.SftpClient;

public class SftpInputStream extends BufferedInputStream {

    private final Log logger = LogFactory.getLog(getClass());

    private SftpClient client;
    private boolean autoDelete = true;
    private String fileName;
    private String baseDir;
    private String archiveDir;
    private boolean postProcessOnClose = true;
    private final AtomicBoolean closed = new AtomicBoolean(false);

    public String getFileName()
    {
        return fileName;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    private boolean errorOccured = false;

    // Log every 10 000 000 bytes read at debug-level
    // Good if really large files are transferred and you tend to get nervous by not
    // seeing any progress in the logfile...
    private static final int LOG_BYTE_INTERVAL = 10000000;
    private long bytesRead = 0;
    private long nextLevelToLogBytesRead = LOG_BYTE_INTERVAL;

    private RlsftpConnectorConfig config;

    public static SftpInputStream createSftpInputStream(RlsftpConnectorConfig config,
        String fileName,
        String baseDir,
        boolean autoDelete,
        String archiveDir) throws IOException, Exception {

            SftpClient client = config.getClient();
            return new SftpInputStream(config,
                client,
                client.retrieveFile(Paths.get(baseDir, fileName).toString()),
                fileName,
                baseDir,
                autoDelete,
                archiveDir);
   }
    /**
     * A special sftp InputStream. The constructor creates the InputStream by calling
     * <code>SftpClient.retrieveFile(fileName)</code>. The client passed in is
     * destroyed when the stream is closed.
     * 
     * @param is The stream that should be used
     * @param fileName name of the file to be retrieved
     * @param autoDelete whether the file specified by fileName should be deleted
     * @param endpointUri the endpoint URI associated to a specific client (connector) pool.
     * @throws Exception if failing to retrieve internal input stream.
     */
    private SftpInputStream(RlsftpConnectorConfig config,
                            SftpClient client,
                           InputStream is,
                           String fileName,
                           String baseDir,
                           boolean autoDelete,
                           String archiveDir) throws Exception
    {
        super(is);
        this.config = config;
        this.client = client;
        this.fileName = fileName;
        this.autoDelete = autoDelete;
        this.baseDir = baseDir;
        this.archiveDir = archiveDir;
    }

    

	@Override
    public synchronized int read() throws IOException
    {
        logReadBytes(1);
        return super.read();
    }

    @Override
    public synchronized int read(byte[] b, int off, int len) throws IOException
    {
        logReadBytes(len);
        return super.read(b, off, len);
    }

    @Override
    public int read(byte[] b) throws IOException
    {
        logReadBytes(b.length);
        return super.read(b);
    }

    @Override
    public void close() throws IOException
    {
        if (closed.compareAndSet(false, true))
        {
            try {
                if (logger.isDebugEnabled())
                {
                    logger.debug("Closing the stream for the file " + fileName);
                }

                try
                {
                    super.close();
                }
                catch (IOException e)
                {
                    logger.error("Error occurred while closing file " + fileName, e);
                    throw e;
                }

                if (postProcessOnClose)
                {
                    try
                    {
                        postProcess();
                    }
                    catch (Exception e)
                    {
                        throw new MuleRuntimeException(e);
                    }
                }
            } finally {
                try {
                    this.config.releaseClient(client);
                } catch (Exception e) {
                    // Do nothing
                }
            }

        }
    }

    public boolean isClosed()
    {
        return closed.get();
    }

    public void postProcess() throws Exception
    {
        if (!client.isConnected())
        {
            return;
        }

        if ( archiveDir != null ) {
            if ( !archiveDir.startsWith("/") ) {
                archiveDir = Paths.get(baseDir, archiveDir).toString();
            }
            SftpUtils.createSftpDirIfNotExists(client, archiveDir);
            String originalPath = Paths.get(baseDir, fileName).toString();
            String archivePath = Paths.get(archiveDir, fileName).toString();
            client.rename(originalPath, archivePath);
        } else if (autoDelete && !errorOccured)
        {
            client.deleteFile(Paths.get(baseDir, fileName).toString());
        }
    }

    public void setErrorOccurred()
    {
        if (logger.isDebugEnabled()) logger.debug("setErrorOccurred() called");
        this.errorOccured = true;
    }

    public void performPostProcessingOnClose(boolean postProcessOnClose)
    {
        this.postProcessOnClose = postProcessOnClose;
    }

    @Override
    public String toString()
    {
        return "SftpInputStream{" + "fileName='" + fileName + '\'' + " from endpoint="
               + config.getConnectionUri() + '}';
    }

    private void logReadBytes(int newBytesRead)
    {
        if (!logger.isDebugEnabled()) return;

        this.bytesRead += newBytesRead;
        if (this.bytesRead >= nextLevelToLogBytesRead)
        {
            logger.debug("Read " + this.bytesRead + " bytes and couting...");
            nextLevelToLogBytesRead += LOG_BYTE_INTERVAL;
        }
    }
    
}
