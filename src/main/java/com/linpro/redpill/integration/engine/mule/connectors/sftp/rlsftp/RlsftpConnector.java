package com.linpro.redpill.integration.engine.mule.connectors.sftp.rlsftp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mule.api.ConnectionException;
import org.mule.api.ConnectionExceptionCode;
import org.mule.api.MessagingException;
import org.mule.api.MuleEvent;
import org.mule.api.annotations.Config;
import org.mule.api.annotations.Connector;
import org.mule.api.annotations.Processor;
import org.mule.api.annotations.ReconnectOn;
import org.mule.api.annotations.Source;
import org.mule.api.annotations.SourceStrategy;
import org.mule.api.annotations.display.Placement;
import org.mule.api.annotations.param.Default;
import org.mule.api.annotations.param.Optional;
import org.mule.api.callback.SourceCallback;
import org.mule.api.transformer.TransformerException;
import org.mule.config.i18n.CoreMessages;
import org.mule.transport.DefaultMuleMessageFactory;
import org.mule.transport.sftp.SftpClient;
import org.mule.transport.sftp.SftpClient.WriteMode;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.linpro.redpill.integration.engine.mule.connectors.sftp.rlsftp.config.RlsftpConnectorConfig;
import com.linpro.redpill.integration.protocol.sftp.SftpInputStream;
import com.linpro.redpill.integration.protocol.sftp.SftpUtils;

@Connector(name = "rlsftp-connector", friendlyName = "Rlsftp")
public class RlsftpConnector {

	private static Logger logger = LogManager.getLogger(RlsftpConnector.class);

	@Config
	private RlsftpConnectorConfig config;

	public RlsftpConnectorConfig getConfig() {
		return config;
	}

	public void setConfig(RlsftpConnectorConfig config) {
		this.config = config;
	}

	@Processor(friendlyName = "Send file")
	@ReconnectOn(exceptions = { ConnectionException.class })
	public void processorSendFiles(@Placement(order = 10) String remotePath, @Placement(order = 20) String fileName,
			@Placement(order = 30) WriteMode mode, MuleEvent event) throws ConnectionException, MessagingException {
		
		SftpClient client = null;
		try {
			client = config.getClient();
		} catch( Exception e) {
			throw new ConnectionException(ConnectionExceptionCode.UNKNOWN,
							"SENDFILES-0",
							"Error storing file " + config.getConnectionUri() + " " + remotePath, e);
		}
		try {
			SftpUtils.createSftpDirIfNotExists(client, remotePath);
			client.storeFile(Paths.get(remotePath, fileName).toString(),
					event.getMessage().getPayload(InputStream.class), mode);
		} catch (TransformerException | IOException e) {
			throw new MessagingException(event, e);
		} finally {
			try {
				config.releaseClient(client);
			} catch (Exception e) {
				// Do nothing
			}
		}
	}

	@Source(friendlyName = "Retrieve files", sourceStrategy = SourceStrategy.POLLING, pollingPeriod = 1000)
	@ReconnectOn(exceptions = { ConnectionException.class })
	public void sourceRetrieveAllFiles(@Placement(order = 10) String remotePath,
			@Placement(order = 20) String filePattern, @Placement(order = 25) @Optional Long fileAge,
			@Placement(order = 30) @Optional String archiveDirectory,
			@Placement(order = 40) @Default("true") boolean autoDelete, final SourceCallback callback)
			throws ConnectionException {

		PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:" + filePattern);
		SftpClient client = null;
		try {
			client = config.getClient();
		} catch( Exception e) {
			throw new ConnectionException(ConnectionExceptionCode.UNKNOWN,
							"SOURCERETRIVEALL-0",
							"Error retrieve files " + config.getConnectionUri() + " " + remotePath, e);
		}
		try {
			List<String> remoteFiles = Arrays.asList(client.listFiles(remotePath));
			if (fileAge != null && fileAge.intValue() > 0) {
				try {
					remoteFiles = SftpUtils.getStableFiles(remotePath, remoteFiles, client, fileAge,
							logger);
				} catch (InterruptedException e) {
					throw new ConnectionException(ConnectionExceptionCode.UNKNOWN,
							"SOURCERETRIVEALL-0",
							"Error getting files " + config.getConnectionUri() + " " + remotePath, e);
				}
			}
			// Release the client once we have the list of files
			try {
				config.releaseClient(client);
			} catch (Exception e) {
				// Do nothing
			}
			for (String file : remoteFiles) {
				if (matcher.matches(Paths.get(file).getFileName())) {
					logger.debug("Matched file: {}", file);
					try { 
						Map<String, Object> properties = new HashMap<String, Object>();
						properties.put("originalFilename", file);
						SftpInputStream stream = SftpInputStream.createSftpInputStream(
							config,
							file,
							remotePath,
							autoDelete,
							archiveDirectory
						);
						callback.process(stream, properties);
					} catch (Exception e) {
						throw new ConnectionException(ConnectionExceptionCode.UNKNOWN,
							"SOURCERETRIVEALL-1",
							"Error processing file " + config.getConnectionUri() + " " + remotePath, e);
					}
				}
			}
		} catch (IOException e) {
			throw new ConnectionException(ConnectionExceptionCode.CANNOT_REACH,
			"SOURCERETRIVEALL-2",
			"Failed to get files from " + config.getConnectionUri() + " " + remotePath, e);
		}
	}

	@Processor(friendlyName = "Retrieve files", intercepting = true)
	@ReconnectOn(exceptions = { ConnectionException.class })
	public void processorRetrieveAllFiles(@Placement(order = 10) String remotePath,
			@Placement(order = 20) String filePattern, @Placement(order = 25) @Optional Long fileAge,
			@Placement(order = 30) @Optional String archiveDirectory,
			@Placement(order = 40) @Default("true") boolean autoDelete, final SourceCallback callback)
			throws ConnectionException {

		PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:" + filePattern);
		SftpClient client = null;
		try {
			client = config.getClient();
		} catch( Exception e) {
			throw new ConnectionException(ConnectionExceptionCode.UNKNOWN,
							"RETRIVEALL-0",
							"Error storing file " + config.getConnectionUri() + " " + remotePath, e);
		}
		try {
			List<String> remoteFiles = Arrays.asList(client.listFiles(remotePath));
			if (fileAge != null && fileAge.intValue() > 0) {
				try {
					remoteFiles = SftpUtils.getStableFiles(remotePath, remoteFiles, client, fileAge,
							logger);
				} catch (InterruptedException e) {
					throw new ConnectionException(ConnectionExceptionCode.UNKNOWN,
							"RETRIVEALL-0",
							"Error getting files " + config.getConnectionUri() + " " + remotePath, e);
				}
			}
			// Release the client once we have the list of files
			try {
				config.releaseClient(client);
			} catch (Exception e) {
				// Do nothing
			}
			for (String file : remoteFiles) {
				if (matcher.matches(Paths.get(file).getFileName())) {
					logger.debug("Matched file: {}", file);
					try { 
						Map<String, Object> properties = new HashMap<String, Object>();
						properties.put("originalFilename", file);
						SftpInputStream stream = SftpInputStream.createSftpInputStream(
							config,
							file,
							remotePath,
							autoDelete,
							archiveDirectory
						);
						callback.process(stream, properties);
					} catch (Exception e) {
						throw new ConnectionException(ConnectionExceptionCode.UNKNOWN,
							"RETRIVEALL-1",
							"Error processing file " + config.getConnectionUri() + " " + remotePath, e);
					}
				}
			}
		} catch (IOException e) {
			throw new ConnectionException(ConnectionExceptionCode.CANNOT_REACH,
			"RETRIVEALL-2",
			"Failed to get files from " + config.getConnectionUri() + " " + remotePath, e);
		}
	}

	@Processor(friendlyName = "Get single file")
	@ReconnectOn(exceptions = { ConnectionException.class })
	public InputStream processorGetSingleFile(
		@Placement(order = 10) String remoteFile,
		@Placement(order = 25) @Optional Long fileAge,
		@Placement(order = 30) @Optional String archiveDirectory,
		@Placement(order = 40) @Default("true") boolean autoDelete) throws ConnectionException {

		SftpClient client = null;
		try {
			client = config.getClient();
		} catch( Exception e) {
			throw new ConnectionException(ConnectionExceptionCode.UNKNOWN,
							"SINGLEFILE-0",
							"Error storing file " + config.getConnectionUri() + " " + remoteFile, e);
		}
		try {
			if (fileAge != null && fileAge.intValue() > 0) {
				List<String> remoteFiles = new ArrayList<>();
				remoteFiles.add(remoteFile);
				try {
					remoteFiles = SftpUtils.getStableFiles("", remoteFiles, client, fileAge,
							logger);
				} catch (InterruptedException e) {
					throw new ConnectionException(ConnectionExceptionCode.UNKNOWN,
							"SINGLEFILE-0",
							"Error getting files " + config.getConnectionUri() + " " + remoteFile, e);
				}
				if ( remoteFiles.size() < 1 ) {
					return null;
				}
				remoteFile = remoteFiles.get(0);
			}
			// Release the client once we have the file
			try {
				config.releaseClient(client);
			} catch (Exception e) {
				// Do nothing
			}
			
			SftpInputStream stream = SftpInputStream.createSftpInputStream(
							config,
							remoteFile,
							"",
							autoDelete,
							archiveDirectory
						);
			return stream;
		} catch (IOException e) {
			throw new ConnectionException(ConnectionExceptionCode.CANNOT_REACH,
			"SINGLEFILE-1",
			"Failed to get file " + remoteFile + " from " + config.getConnectionUri(), e);
		} catch (Exception e) {
			throw new ConnectionException(ConnectionExceptionCode.CANNOT_REACH,
			"SINGLEFILE-2",
			"Failed to get file " + remoteFile + " from " + config.getConnectionUri(), e);
		}
	}

	@Processor(friendlyName = "Move remote files")
	@ReconnectOn(exceptions = { ConnectionException.class })
	public void processorMoveRemoteFiles(
		@Placement(order = 10) String remoteSourcePath,
		@Placement(order = 20) String filePattern,
		@Placement(order = 25) @Optional Long fileAge,
		@Placement(order = 30) String remoteDestinationPath) throws ConnectionException {

		PathMatcher matcher = FileSystems.getDefault().getPathMatcher(
			"glob:" + filePattern);
		SftpClient client = null;
		try {
			client = config.getClient();
		} catch( Exception e) {
			throw new ConnectionException(ConnectionExceptionCode.UNKNOWN,
							"MOVE-0",
							"Error moving file " + config.getConnectionUri() + " " + filePattern 
							+ " from " + " " + remoteSourcePath + " to " + remoteDestinationPath, e);
		}
		try {
			for (String file : client.listFiles(remoteSourcePath)) {
				if (matcher.matches(Paths.get(file).getFileName())) {
					logger.debug("Matched file: {}", file);
					SftpUtils.createSftpDirIfNotExists(client, remoteDestinationPath);
					try { 
						client.rename(
							Paths.get(remoteSourcePath, file).toString(), 
							Paths.get(remoteDestinationPath, file).toString());
					} catch (Exception e) {
						throw new ConnectionException(ConnectionExceptionCode.UNKNOWN,
							"MOVE-0",
							"Error moving file " + config.getConnectionUri() + " " + Paths.get(remoteSourcePath, file).toString()
							+ " to " + Paths.get(remoteDestinationPath, file).toString(), e);
					}
				}
			}
		} catch (IOException e) {
			throw new ConnectionException(ConnectionExceptionCode.CANNOT_REACH,
			"MOVE-1",
			"Failed to move files from " + config.getConnectionUri() + " " + remoteSourcePath + " to " + remoteDestinationPath, e);
		} finally {
			try {
				config.releaseClient(client);
			} catch (Exception e) {
				// Do nothing
			}
		}
	}
}