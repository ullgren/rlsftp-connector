package com.linpro.redpill.integration.engine.mule.connectors.sftp.rlsftp.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.mule.api.annotations.Disconnect;

import org.mule.api.annotations.ValidateConnection;
import org.mule.transport.sftp.SftpClient;

import org.apache.commons.pool.PoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;

public abstract class RlsftpConnectorConfig implements PoolableObjectFactory<SftpClient> {

	protected final static Log logger = LogFactory.getLog(RlsftpConnectorConfig.class);

	private GenericObjectPool<SftpClient> clientPool;
	private String connectionUri;

	private String host;

	private String port;

	private String username;

	private Object lock = new Object();

	/**
	 * max pool size. 0 for no pool, -1 for no limit, otherwise the specified value
	 */
	private int maxConnectionPoolSize;

	public SftpClient getClient() throws Exception {
		synchronized (lock) {
			if (clientPool == null) {
				clientPool = new GenericObjectPool<SftpClient>(this, getMaxConnectionPoolSize());
				clientPool.setTestOnBorrow(true);
			}
		}
		return (SftpClient) clientPool.borrowObject();
	}

	public void releaseClient(SftpClient client) throws Exception {
		if (client != null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Releasing connection for endpoint " + this.getConnectionUri());
			}
			if (!client.isConnected()) {
				clientPool.invalidateObject(client);
			} else {
				clientPool.returnObject(client);
			}
		}
	}

	public String getConnectionUri() {
		return connectionUri;
	}

	public void setConnectionUri(String connectionUri) {
		this.connectionUri = connectionUri;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getMaxConnectionPoolSize() {
		return maxConnectionPoolSize;
	}

	public void setMaxConnectionPoolSize(int maxConnectionPoolSize) {
		this.maxConnectionPoolSize = maxConnectionPoolSize;
	}

	@Disconnect
	public void disconnect() {
		/*
		 * clientPool. if (client != null) { client.disconnect(); }
		 */
		// TODO: Clear the pool
	}

	@ValidateConnection
	public boolean isConnected() {
		// If we do not have a clientPool we can not be connected.
		if ( clientPool == null ) {
			return false;
		}
		boolean ret = false;
		SftpClient client = null;
		try {
			client = getClient();
			if (client != null) {
				ret = client.isConnected();
			}
		} catch (Exception e) {
			logger.error("Failed to validate connection. Caused by " + e.getMessage(), e);
			return false;
		} finally {
			try {
				releaseClient(client);
			} catch (Exception e) {
				// Do nothing
			}
		}
		return ret;
	}

	@Override
	public void activateObject(SftpClient arg0) throws Exception {
		// Do nothing

	}

	@Override
	public void destroyObject(SftpClient client) throws Exception {
		client.disconnect();
	}

	@Override
	public void passivateObject(SftpClient client) throws Exception {
		// Do nothing!

	}

	@Override
	public boolean validateObject(SftpClient client) {
        if (logger.isDebugEnabled())
        {
            logger.debug("Inside validateObject - will return " + client.isConnected());
        }
        return client.isConnected();
	}
}
