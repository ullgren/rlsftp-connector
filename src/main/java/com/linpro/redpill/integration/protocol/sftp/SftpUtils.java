package com.linpro.redpill.integration.protocol.sftp;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.mule.transport.sftp.SftpClient;

public abstract class SftpUtils {

    private static final Object lock = new Object();

    public static void createSftpDirIfNotExists(SftpClient client, String newDir) throws IOException {

        // We need to have a synchronized block if two++ threads tries to
        // create the same directory at the same time
        synchronized (lock) {
            // Try to change directory to the new dir, if it fails - create it
            try {
                // This method will throw an exception if the directory does not
                // exist.
                client.changeWorkingDirectory(newDir);
            } catch (IOException e) {
                client.mkdir(newDir);
            }
        }
    }

    public static List<String> getStableFiles(String baseDir, List<String> fileNames, SftpClient client, long sizeCheckDelayMs, Logger logger) throws InterruptedException
    {
        List<String> stableFiles = new ArrayList<>(fileNames.size());
        Map<String, Long> fileSizesBeforeDelay = getFileTimeStamps(baseDir, fileNames, client, logger);
        Thread.sleep(sizeCheckDelayMs);
        Map<String, Long> fileSizesAfterDelay = getFileTimeStamps(baseDir, fileNames, client, logger);

        for (Map.Entry<String, Long> sizeEntry : fileSizesBeforeDelay.entrySet())
        {
            Long sizeBeforeDelay = sizeEntry.getValue();
            Long sizeAfterDelay = fileSizesAfterDelay.get(sizeEntry.getKey());
            if (sizeBeforeDelay.equals(sizeAfterDelay))
            {
                stableFiles.add(sizeEntry.getKey());
            }
        }

        return stableFiles;
    }

    private static Map<String, Long> getFileTimeStamps(String baseDir, List<String> fileNames, SftpClient client, Logger logger)
    {
        Map<String, Long> sizes = new HashMap<>();

        for (String fileName : fileNames)
        {
            try
            {
                sizes.put(fileName, client.getSize(
                    Paths.get(baseDir, fileName).toString()));
            }
            catch (IOException e)
            {
                if (logger.isDebugEnabled())
                {
                    logger.debug(String.format("Cannot get the size of file '%s'", fileName));
                }
            }
        }

        return sizes;
    }

}
