# Rlsftp Anypoint Connector

# Supported versions, was compiled and run under:
Mule 3.9.0
DevKit Extension 1.1.5.201608221430 

# Destination application supported versions:
SFTP server

# Installation 
For beta connectors you can download the source code and build it with devkit to find it available on your local repository. Then you can add it to Studio.

For released connectors you can download them from the update site in Anypoint Studio. 
Open Anypoint Studio, go to Help and Install New Software and select Anypoint Connectors Update Site where you'll find all avaliable connectors.

# Usage
For information about usage our documentation at http://github.com/mulesoft/rlsftp.

# Reporting Issues
We use GitHub:Issues for tracking issues with this connector. You can report new issues at this link http://github.com/mulesoft/rlsftp/issues.