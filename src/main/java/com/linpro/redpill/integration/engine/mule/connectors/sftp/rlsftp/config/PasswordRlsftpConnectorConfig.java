package com.linpro.redpill.integration.engine.mule.connectors.sftp.rlsftp.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mule.api.ConnectionException;
import org.mule.api.ConnectionExceptionCode;
import org.mule.api.annotations.Configurable;
import org.mule.api.annotations.Connect;
import org.mule.api.annotations.ConnectStrategy;
import org.mule.api.annotations.ConnectionIdentifier;
import org.mule.api.annotations.Disconnect;

import org.mule.api.annotations.TestConnectivity;
import org.mule.api.annotations.ValidateConnection;
import org.mule.api.annotations.components.ConnectionManagement;
import org.mule.api.annotations.display.Password;
import org.mule.api.annotations.param.ConnectionKey;
import org.mule.api.annotations.param.Default;
import org.mule.api.annotations.param.Optional;
import org.mule.transport.sftp.SftpClient;


@ConnectionManagement(friendlyName = "Password authentication", 
  configElementName = "passwordauth-config")
public class PasswordRlsftpConnectorConfig extends RlsftpConnectorConfig {

	private static Logger logger = LogManager.getLogger(RlsftpConnectorConfig.class);

	private String connectionUri;

	private String userpassword;

	public String getConnectionUri() {
		return connectionUri;
	}

	public void setConnectionUri(String connectionUri) {
		this.connectionUri = connectionUri;
	}

	public String getUserpassword() {
		return userpassword;
	}
	public void setUserpassword(String userpassword) {
		this.userpassword = userpassword;
	}


	@Connect(strategy = ConnectStrategy.SINGLE_INSTANCE)
	@TestConnectivity
	public void connect(@ConnectionKey @Default("localhost") String host,
		@ConnectionKey @Default("22") String port, 
		@ConnectionKey String username, 
		@Password String userpassword,
		@Default("2") String maxConnectionPoolSize) throws ConnectionException {
			
			this.setHost(host);
			this.setPort(port);
			this.setUsername(username);
			this.setUserpassword(userpassword);
			this.setMaxConnectionPoolSize(Integer.valueOf(maxConnectionPoolSize).intValue());
			setConnectionUri(getUsername() + "@" + getHost() + ":" + getPort());

			try {
				SftpClient client = getClient();
				releaseClient(client);
			} catch (Exception e) {
				logger.error("Could not login to sftp server on {}", connectionUri);
				throw new ConnectionException(ConnectionExceptionCode.CANNOT_REACH,
						"Could not connect to sftp server on " + getConnectionUri(), e.getMessage());
			}
	}

	@Override
	public SftpClient makeObject() throws Exception {
		SftpClient client = new SftpClient(getHost());
		client.setPort(Integer.valueOf(getPort()));
		try {
			client.login(
				this.getUsername(),
				this.getUserpassword());
			logger.info("Rlsftp (" + getConnectionUri() + ") connected");
		} catch (Exception e) {
			logger.error("Could not login to sftp server on {}", connectionUri);
			throw e;
		}
		return client;
	}
}
