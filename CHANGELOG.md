# Rlsftp Release Notes

# Date: 28-Mar-2018
# Version: 1.2.0-SNAPSHOT

# Supported Mule Runtime Versions, was compiled and run under:
Mule 3.9.0
DevKit Extension 1.1.5.201608221430 

# New Features and Functionality
Remote retrieve files with support for file filter pattern.
Remote delete file on successful file transfer.
Remote move file on successful file transfer.
Remote user script for connection and command.
